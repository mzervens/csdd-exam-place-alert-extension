
console.log("Hello from your Chrome extension!");
var matchFound = false;
chrome.runtime.onMessage.addListener(find_csdd_dates);

chrome.storage.sync.get(['csdd_dates'], function(result){
    if(!chrome.runtime.error && result.csdd_dates){
		match_dates(JSON.parse(result.csdd_dates));
    }
});

function find_csdd_dates(message, sender, sendresponse) {
	
	chrome.storage.sync.set({'csdd_dates' : JSON.stringify(message)}, function(){
		if(chrome.runtime.error){
			console.log("Error.");
		}
	});
	
	var datesOfInterest = message;
	match_dates(datesOfInterest);
}



function match_dates(datesOfInterest)
{
    $("#datums > option").each(function()
    {
        var optionText = this.text;
        var isInterest = false;
        var parts = optionText.split(":");

        for(var i = 0; i < datesOfInterest.length; i++)
        {
            if(optionText.indexOf(datesOfInterest[i]) != -1)
            {
                isInterest = true;
                break;
            }
        }

        if(isInterest)
        {
            alert_if_free(parts);
        }

    });
	
	if (!matchFound) {
		setTimeout(function(){
			$("#find").click();
		}, 30000);
	}
}


function alert_if_free(parts)
{
    var dayLabel = parts[0];
    var freeSpots = parts[1].trim();

    console.log(dayLabel, freeSpots);

    if(freeSpots != '0')
    {
        alert_self(dayLabel + " " + freeSpots + " " + new Date);
        console.log("free spots");
		matchFound = true;
    }
    else
    {
        console.log("no free spots");
    }

}


function alert_self(message)
{
	alert(message);
}
